let computerScore = 0;
let playerScore = 0;
let results = document.querySelector("#results");
let p1 = document.querySelector("#p1");
let p2 = document.querySelector("#p2");

//generates computers results
function computerPlay() {
  let move = Math.floor((Math.random() * 3) + 1);

  switch (move) {
    case 1:
      return "rock";

    case 2:
      return "paper";

    case 3:
      return "scissors";

    default:
      return null;
  }
}

function win(playerSelection, computerSelection) {
  playerScore += 1;
  results.textContent = "You win, " + playerSelection + " beats " +
    computerSelection;
}

function lose(playerSelection, computerSelection) {
  computerScore += 1;
  results.textContent = "You lose, " + computerSelection + " beats " +
    playerSelection;
}

//compares results and adds scores
function playRound(playerSelection) {
  let computerSelection = computerPlay();

  if ((playerSelection === "rock" && computerSelection == "scissors") ||
    (playerSelection === "paper" && computerSelection == "rock") ||
    (playerSelection === "scissors" && computerSelection == "paper")) {
    win(playerSelection, computerSelection);
  } else if ((playerSelection === "rock" && computerSelection == "paper") ||
    (playerSelection === "paper" && computerSelection == "scissors") ||
    (playerSelection === "scissors" && computerSelection == "rock")) {
    lose(playerSelection, computerSelection);
  } else if (playerSelection === computerSelection) {
    results.textContent = "It's a tie!";
  }
}

//define buttons
const buttons = document.querySelectorAll('button');

buttons.forEach((button) => {
  button.addEventListener('click', (e) => {
    playRound(button.innerHTML.toLowerCase());
    checkScores();
  });
});

function removePlayButtons() {
  rock.remove();
  paper.remove();
  scissors.remove();
}

//check player scores
function checkScores() {
  p1.textContent = "Player: " + playerScore;
  p2.textContent = "Computer: " + computerScore;

  if (playerScore >= 5) {
    results.textContent = "You win!";
    removePlayButtons();
  } else if (computerScore >= 5) {
    results.textContent = "The computer wins";
    removePlayButtons();
  }
}
